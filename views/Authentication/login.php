<?php
session_start();
include_once('../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Auth();
$status= $auth->prepare($_POST)->is_registered();
if($status){
    $_SESSION['user_email']=$_POST['email'];
    return Utility::redirect('../welcome.php');
}else{
    Message::message("Wrong username or password");
    return Utility::redirect('../../index.php');

}
